(function ($) {

/**
 * Loads media browsers and callbacks, specifically for media as a field.
 */
Drupal.behaviors.mediaInsert = {
  attach: function (context, settings) {
    // Options set from media.fields.inc for the types, etc to show in the browser.
    
    // For each widget (in case of multi-entry)
    $('.media-widget', context).once('mediaInsertButton', function () {
      
      $('<a class="media-insert" href="#">' + Drupal.t('Insert') + '</a>')
        .click(mediaInsertClick)
        .appendTo(this);
      
    });
  }
};

function mediaInsertClick() {

  var body = $('#edit-body textarea.text-full');
  var widget = $(this).parent('.media-widget');
  var fid = widget.find('.fid').val();
  var content = body.val();

  if (fid > 0) {

    body.val(content + '[[{"fid":' + fid + '}]]');

  } // if
  
  return false;

} // mediaInsertClick

})(jQuery);
